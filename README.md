# Recherche_logiciel

Script qui renvoi vers la page de téléchargement du logiciel choisit.

/!\ Ne sont présent dans le dictionnaire-index que les logiciels qui j'utilise, il faut bien évidemment ajuster en fonction de vos besoins

Le fichier liste.txt est à placer dans le même dossier que le script. Il contient la liste des logiciels ainsi que leur page de téléchargement sous la forme :

`<NOM_LOGICIEL>_<URL_TELECHARGEMENT>`

A vous ensuite de la modifier à vos besoin.

Une fois dans le champ de recherche, pas la peine de taper le nom du logiciel en entier, ni de s'occuper de la casse (MAJ/min).

*Exemple : si je tape 'ink' le script va alors m'ouvrir la page d'Inkscape, mais si je tape 'pi' dans mon cas j'ai alors 2 pages qui vont s'ouvrir : PiP et PINN*