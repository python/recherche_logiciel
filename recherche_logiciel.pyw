# -*- coding: utf-8 -*-

from tkinter import *
from tkinter.messagebox import *
import webbrowser
from pathlib import Path

#Fonction d'erreur si le fichier n'est pas présent
def ErreurFile() :
        print("Fichier non trouvé / non valide. Sortie du programme...")
        sys.exit(0)
        
#On verifie si le fichier est valide
def CheckFile(file) :
        dicoFile=Path(file)
        if dicoFile.is_file()==False : ErreurFile()

#Fonction qui va charger le fichier et le transformer en dictionnaire
def ChargerFile(file) :
        with open(file, "r") as file: lignes=file.read().splitlines()
        dico={}
        for elem in lignes :
                data=elem.split("_")
                dico[data[0]]=data[1]
        if dico=={} : ErreurFile()
        else : return dico

#Fonction d'erreur si le logiciel n'est pas trouve
def Erreur() :
        showwarning('Résultat','Logiciel non trouvé.\nVeuillez recommencer !')
        recherche.set('')

#Fonction qui va rechercher le logiciel dans le dictionnaire puis ouvrir la page de download
def Recherche() :
    if recherche.get()==('') : Erreur()
    else :
        trouve=False
        for key in ind.keys() :
            if recherche.get().lower() in key.lower():
                webbrowser.open(ind[key])
                trouve=True
        if not trouve : Erreur()
        recherche.set('')

#Fonction qui sert a lancer la Recherche() en appuyant simplement la touche <Entree>
def callback(event) :
        Recherche()
        
#On verifie que le fichier existe puis on charge le dictionnaire
CheckFile("liste.txt")
ind=ChargerFile("liste.txt")
                

#Declaration de la fenetre Tkinter
root=Tk()
root.title("Recherche logiciel")
root['bg']='grey'

#Label "Logiciel"
Label1=Label(root,text='Logiciel : ')
Label1.pack(side=LEFT,padx=5,pady=5)

#Champ de recherche
recherche=StringVar()
Champ=Entry(root,textvariable=recherche,bg ='bisque',fg='maroon')
Champ.focus_set()
Champ.pack(side=LEFT,padx=5,pady=5)
Champ.bind("<Return>",callback)

#Bouton de validation qui lance la fonction de Recherche()
Bouton = Button(root,text='Valider',command=Recherche)
Bouton.pack(side=LEFT,padx=5,pady=5)

#Bouton pour quitter
Bouton_quit=Button(root,text='Quitter',command=root.destroy,relief=FLAT,bg='blue')
Bouton_quit.pack(side=LEFT,padx=15,pady=5)

root.mainloop()